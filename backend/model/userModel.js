const dotenv = require("dotenv");
dotenv.config({ path: "backend/config/config.env" });
const pool = require("./../config/db");

module.exports = {
    getUser: (data, callBack) => {
        const sql = `SELECT * FROM users WHERE email = ? AND password = ?`;
        pool.query(
            sql, [data.email, data.password], (err, results) => {
                if (err) {
                    return callBack(err);
                }
                return callBack(null, results);
            }
        )
    },

    registerUser: async (data, callBack) => {
        const usrExists = `SELECT COUNT(*) AS usercount FROM users WHERE email = ?`;
        pool.query(usrExists, [data.email, data.password], (err, results) => {
            if (err) { return callBack(err); }
            if (results[0].usercount > 0) {
                return callBack(null, results);
            } else {
                const sql = `INSERT INTO users(name, email, password) VALUES (?,?,?)`;
                pool.query(sql, [data.name, data.email, data.password], (err, results) => {
                    if (err) { return callBack(err); }
                    pool.query(`SELECT id, name, email FROM users WHERE id=?`, [results.insertId], (err, result) => {
                        if (err) { return callBack(err); }
                        return callBack(null, result);
                    });
                });
            }
        });
    },

    getEvent: async (id, callBack) => {
        const sql = `SELECT * FROM user_events WHERE user_id = ?`;
        pool.query(sql, [id], (err, results) => {
            if (err) { return callBack(err); }
            return callBack(null, results);
        });
    },

    createEvent: async (data, callBack) => {
        const sql = `INSERT INTO user_events(user_id, name, date, description, price, booking, acceptance) VALUES (?,?,?,?,?,?,?)`;
        pool.query(sql, [data.userId, data.name, data.date, data.description, data.price, data.booking, data.conditions], (err, results) => {
            if (err) { return callBack(err); }
            return callBack(null, results);
        });
    },

    editEvent: async (data, callBack) => {
        let sql = "UPDATE user_events SET";
        let holder = {};
        let sqlBind = [];

        Object.keys(data).map((key) => {
            if (data[key] !== "undefined" && key !== "id" && key !== "userId") {
                holder[key] = data[key];
            }
        });

        if (Object.keys(holder).length) {
            const rowLen = Object.keys(holder).length;
            Object.keys(holder).map((val, key) => {
                sqlBind.push(holder[val]);
                if (rowLen === key + 1) {
                    sql += ` ${val} = ?`;
                } else {
                    sql += ` ${val} = ?,`;
                }
            });
            sqlBind.push(data.id); sql += " WHERE id = ?";
            pool.query(sql, sqlBind, (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            });
        }
    },

    deleteEvent: async (id, callBack) => {
        const sql = "DELETE FROM user_events WHERE id = ?";
        pool.query(sql, [id], (err, results) => {
            if (err) callBack(err);
            return callBack(null, results.affectedRows);
        });
    }
}