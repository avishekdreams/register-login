const router = require('express').Router();
const {
    getUser,
    registerUser,
    userLogout,
    createEvent,
    getEvent,
    editEvent,
    deleteEvent
} = require("../controller/userController");


router.post('/register', registerUser);
router.post('/signin', getUser);
router.post('/event/get', getEvent);
router.post('/event/create', createEvent);
router.put('/event/edit', editEvent);
router.put('/event/delete', deleteEvent);

// router.get('/signout', userLogout);

module.exports = router;