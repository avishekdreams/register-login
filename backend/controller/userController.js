
const { getUser, registerUser, createEvent, getEvent, editEvent, deleteEvent } = require("../model/userModel");

module.exports = {
    getUser: (req, res, next) => {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.json({ success: false, message: "Please enter your email and password" });
        }
        try {
            const data = { "email": email, "password": password };
            getUser(data, async (err, results) => {
                if (err) { return next(error); }
                if (results.length === 0) { return res.json({ success: false, message: 'Username or password is incorrect' }); }
                if (results.length > 0) {
                    return res.json({ success: true, data: results });
                }

            });
        } catch (error) {
            return next(error);
        }
    },

    registerUser: async (req, res, next) => {
        let { email, name, password } = req.body;
        let data = {
            "name": name,
            "email": email,
            "password": password,
        };
        try {
            registerUser(data, (error, results) => {
                if (error) { return next(error); }
                const result = JSON.parse(JSON.stringify(results));
                console.log(result);
                if (result[0].usercount > 0) {
                    return res.json({ success: false, message: 'User already exists' });
                } else {
                    return res.json({ success: true, data: result });
                }
            });
        } catch (error) {

        }
    },

    createEvent: (req, res, next) => {
        const { userId, name, date, description, price, booking, conditions } = req.body;
        let data = {
            "userId": userId,
            "name": name,
            "date": date,
            "description": description,
            "price": price,
            "booking": booking ? "premium" : "normal",
            "conditions": conditions ? 1 : 0
        };
        try {
            createEvent(data, (error, results) => {
                if (error) { return next(error); }
                const result = JSON.parse(JSON.stringify(results));
                if (result.affectedRows > 0) {
                    return res.json({ success: true, message: "Data inserted successfully" });
                }
            });
        } catch (error) {

        }
    },

    getEvent: (req, res, next) => {
        const { id } = req.body;
        try {
            getEvent(id, (error, results) => {
                if (error) { return next(error); }
                const result = JSON.parse(JSON.stringify(results));
                if (result.length > 0) {
                    return res.json({ success: true, data: result });
                }
            });
        } catch (error) {

        }
    },

    editEvent: (req, res, next) => {
        const { userId, id, name, date, description, price, booking, acceptance } = req.body;
        let data = {
            "userId": userId,
            "id": id,
            "name": name,
            "date": date,
            "description": description,
            "price": price,
            "booking": booking,
            "acceptance": acceptance === "true" ? 1 : 0
        };
        try {
            editEvent(data, (error, results) => {
                if (error) { return next(error); }
                const result = JSON.parse(JSON.stringify(results));
                if (result.affectedRows > 0) {
                    return res.json({ success: true, message: "Data updated successfully" });
                }
            });
        } catch (error) {

        }
    },

    deleteEvent: (req, res) => {
        const { id } = req.body;
        try {
            deleteEvent(id, (err, results) => {
                if (err) { console.log(err); return; }
                if (results > 0) {
                    return res.json({
                        success: true,
                        message: "Event removed successfully"
                    });
                } else {
                    return res.json({
                        success: false,
                        message: "No such event found"
                    });
                }
            });
        } catch (error) {
            console.log(error);
        }
    },
}