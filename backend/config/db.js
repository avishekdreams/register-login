const mysql = require("mysql");
const dotenv = require("dotenv");
dotenv.config({ path: "backend/config/config.env" });

const pool = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.MYSQL_DB,
});

pool.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});


module.exports = pool;