import React, { useEffect, useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { editEvent } from "../../actions/userAction";
import { useLocation } from "react-router-dom";
import moment from 'moment';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import "./createevent.css";

export default function EditEvents() {
    const { state } = useLocation();
    const dispatchEdit = useDispatch();
    const { userData } = useSelector(state => state.user);
    const { success, message } = useSelector(state => state.event);
    const dateForPicker = (dateString) => {
        return moment(new Date(dateString)).utc().format('YYYY-MM-DD');
    };

    const initialState = {
        id: state.id,
        name: state.name,
        date: state.date,
        description: state.description,
        price: state.price,
        booking: state.booking,
        acceptance: state.conditions
    };

    const formReducer = (redState, action) => {
        switch (action.type) {
            case "HANDLE_INPUT_TEXT":
                return {
                    ...redState,
                    [action.field]: action.payload
                }
            case "TOGGLE_ACCEPTANCE": {
                return {
                    ...redState,
                    acceptance: !redState.acceptance
                };
            }
            default: return redState;
        }
    }

    const navigate = useNavigate();
    const [formState, dispatch] = useReducer(formReducer, initialState);

    const handleChange = (e) => {
        if (e.target.name !== "acceptance") {
            dispatch({
                type: "HANDLE_INPUT_TEXT",
                field: e.target.name,
                payload: e.target.value
            });
        } else {
            dispatch({
                type: "TOGGLE_ACCEPTANCE",
            });
        }
        
    }

    const submitEvent = (e) => {
        e.preventDefault();
        const myForm = new FormData();
        myForm.set("userId", userData.data[0].id);
        myForm.set("id", formState.id);
        myForm.set("name", formState.name);
        myForm.set("date", moment(new Date(formState.date)).utc().format('YYYY-MM-DD'));
        myForm.set("description", formState.description);
        myForm.set("price", formState.price);
        myForm.set("booking", formState.booking);
        myForm.set("acceptance", formState.acceptance);
        dispatchEdit(editEvent(myForm));
    }

    useEffect(() => {
        if (success) {
            navigate("/");
        }
    }, [success, message]);

    return (
        <div className="createEventContainer">
            <Form onSubmit={submitEvent}>
                <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridName">
                        <Form.Label>Event Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="name"
                            value={formState.name}
                            onChange={(e) => handleChange(e)}
                        />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridDate">
                        <Form.Label>Event Date</Form.Label>
                        <Form.Control
                            type="date"
                            name="date"
                            value={dateForPicker(formState.date)}
                            onChange={(e) => handleChange(e)}
                        />
                    </Form.Group>
                </Row>

                <Form.Group className="mb-3" controlId="formGridDescription">
                    <Form.Label>Event Description</Form.Label>
                    <Form.Control
                        type="textarea"
                        name="description"
                        value={formState.description}
                        onChange={(e) => handleChange(e)}
                    />
                </Form.Group>

                <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="number"
                            name="price"
                            value={formState.price}
                            onChange={(e) => handleChange(e)}
                        />
                    </Form.Group>
                </Row>

                <Form.Group className="mb-3" id="formGridBooking" onChange={(e) => handleChange(e)}>
                    <Form.Check
                        inline
                        label="Normal"
                        name="booking"
                        value="normal"
                        defaultChecked={formState.booking === "normal" ? true : false}
                        type="radio"
                        id={`inline-radio-2`}
                    />
                    <Form.Check
                        inline
                        label="Premium"
                        name="booking"
                        value="premium"
                        defaultChecked={formState.booking === "premium" ? true : false}
                        type="radio"
                        id={`inline-radio-3`}
                    />
                </Form.Group>

                <Form.Group className="mb-3" id="formGridCheckbox">
                    <Form.Check
                        type="checkbox"
                        label="I accept terms & conditions"
                        name="acceptance"
                        onChange={(e) => handleChange(e)}
                        defaultChecked={formState.acceptance === 1 ? true : false}
                    />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    )
}