import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createEvent } from "../../actions/userAction";
import moment from 'moment';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import "./createevent.css";

export default function CreateEvents() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const eventNameRef = useRef("");
    const eventDateRef = useRef("");
    const eventDescriptionRef = useRef("");
    const eventPriceRef = useRef("");
    const bookingRef = useRef("");
    const conditionsRef = useRef("");
    const { userData } = useSelector(state => state.user);
    const { createdData } = useSelector(state => state.event);

    const submitEvent = (e) => {
        e.preventDefault();

        const myForm = new FormData();
        myForm.set("userId", userData.data[0].id);
        myForm.set("name", eventNameRef.current.value);
        myForm.set("date", eventDateRef.current.value ? eventDateRef.current.value : moment().format("YYYY-MM-DD"));
        myForm.set("description", eventDescriptionRef.current.value);
        myForm.set("price", eventPriceRef.current.value);
        myForm.set("booking", bookingRef.current.checked);
        myForm.set("conditions", conditionsRef.current.checked);
        dispatch(createEvent(myForm));
    }

    useEffect(() => {
        if (createdData?.success) {
            navigate("/");
        }
    }, [createdData]);

    return (
        <div className="createEventContainer">
            <Form onSubmit={submitEvent}>
                <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridName">
                        <Form.Label>Event Name</Form.Label>
                        <Form.Control type="text" name="eventName" ref={eventNameRef} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridDate">
                        <Form.Label>Event Date</Form.Label>
                        <Form.Control type="date" name="eventDate" ref={eventDateRef} />
                    </Form.Group>
                </Row>

                <Form.Group className="mb-3" controlId="formGridDescription">
                    <Form.Label>Event Description</Form.Label>
                    <Form.Control type="textarea" name="eventDescription" ref={eventDescriptionRef} />
                </Form.Group>

                <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" name="eventPrice" ref={eventPriceRef} />
                    </Form.Group>
                </Row>

                <Form.Group className="mb-3" id="formGridBooking">
                    <Form.Check
                        inline
                        label="Normal"
                        name="booking"
                        value="normal"
                        type="radio"
                        id={`inline-radio-2`}
                        ref={bookingRef}
                    />
                    <Form.Check
                        inline
                        label="Premium"
                        name="booking"
                        value="premium"
                        type="radio"
                        id={`inline-radio-3`}
                        ref={bookingRef}
                    />
                </Form.Group>

                <Form.Group className="mb-3" id="formGridCheckbox">
                    <Form.Check type="checkbox" label="I accept terms & conditions" name="conditions" ref={conditionsRef} />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    )
}