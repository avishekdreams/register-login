import React, { Fragment, useEffect, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { register, login, getEvent, deleteEvent } from "../../actions/userAction";
import UserEvents from "../Home/UserEvents";
import ConfirmModal from "../ConfirmModal";
import { Spinner } from 'react-bootstrap';
import "./../style.css";

const Home = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { loading, isAuthenticated, userData } = useSelector(state => state.user);
    const { eventData, deletedData, loadingEvent } = useSelector(state => state.event);
    const loginTab = useRef(null);
    const registerTab = useRef(null);
    const switcherTab = useRef(null);
    const [loginEmail, setLoginEmail] = useState("su2015jika@gmail.com");
    const [loginPassword, setLoginPassword] = useState("April@1987");
    const [errors, setErrors] = useState({});
    const nameRef = useRef("");
    const emailRef = useRef("");
    const confirmMsg = useRef("");
    const passwordRef = useRef("");
    const [show, setShow] = useState(false);
    const deleteId = useRef(0);

    useEffect(() => {
        if (isAuthenticated) {
            const myForm = new FormData();
            myForm.set("id", userData.data[0].id);
            dispatch(getEvent(myForm));
        }
    }, [isAuthenticated, deletedData]);

    const loginSubmit = (e) => {
        e.preventDefault();
        dispatch(login(loginEmail, loginPassword));
    }

    const registerSubmit = (e) => {
        e.preventDefault();
        let errors = {};
        let reg = new RegExp('^(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*|[a-zA-Z0-9]*)$');

        if (reg.test(passwordRef.current.value)) {
            errors["password"] = 'Password should contain min 8 chars with at least 1 uppercase and 1 special character';
            setErrors(errors);
        } else {
            setErrors({});
        }

        if (Object.keys(errors).length <= 0) {
            const myForm = new FormData();
            myForm.set("name", nameRef.current.value);
            myForm.set("email", emailRef.current.value);
            myForm.set("password", passwordRef.current.value);
            dispatch(register(myForm));
        }
    }

    const switchTabs = (e, tab) => {
        if (tab === "login") {
            switcherTab.current.classList.add("shiftToNeutral");
            switcherTab.current.classList.remove("shiftToRight");

            registerTab.current.classList.remove("shiftToNeutralForm");
            loginTab.current.classList.remove("shiftToLeft");
        }
        if (tab === "register") {
            switcherTab.current.classList.add("shiftToRight");
            switcherTab.current.classList.remove("shiftToNeutral");

            registerTab.current.classList.add("shiftToNeutralForm");
            loginTab.current.classList.add("shiftToLeft");
        }
    }

    const editHandler = (e) => {
        navigate("/events/edit", {
            state: {
                id: e.id,
                name: e.name,
                date: e.date,
                description: e.description,
                price: e.price,
                booking: e.booking,
                conditions: e.acceptance
            }
        });
    }

    const deleteHandler = (e) => {
        deleteId.current = e;
        confirmMsg.current = "Are you sure you want to delete?";
        setShow(true);
    }

    const handleClose = () => setShow(false);

    const handleConfirm = () => {
        const myForm = new FormData();
        myForm.set("id", deleteId.current);
        dispatch(deleteEvent(myForm));
        setShow(false);
    }

    if (loading) {
        return <div className="LoginSignUpContainer"><Spinner animation="border" style={{ width: "8rem", height: "8rem" }} /></div>;
    }

    return (
        <Fragment>
            <div className="LoginSignUpContainer">
                {isAuthenticated ? (
                    <Fragment>
                        {!loadingEvent ? (
                            <div>
                                <h3>Welcome {userData.data[0].name}</h3>
                                <UserEvents events={eventData?.data} editHandler={(e) => editHandler(e)} deleteHandler={(e) => deleteHandler(e)} />
                                <Link to="/events/create">Create Events</Link>
                            </div>
                        ) : (<Spinner animation="border" style={{ width: "8rem", height: "8rem" }} />)}
                    </Fragment>
                ) : (
                    <div className="LoginSignUpBox">
                        <div>
                            <div className="login_signUp_toggle">
                                <p onClick={(e) => switchTabs(e, "login")}>LOGIN</p>
                                <p onClick={(e) => switchTabs(e, "register")}>REGISTER</p>
                            </div>
                            <button ref={switcherTab}></button>
                        </div >
                        <form className="loginForm" ref={loginTab} onSubmit={loginSubmit}>
                            <div className="loginEmail">
                                <input
                                    type="email"
                                    placeholder="Email"
                                    required
                                    value={loginEmail}
                                    onChange={(e) => setLoginEmail(e.target.value)}
                                />
                            </div>
                            <div className="loginPassword">
                                <input
                                    type="password"
                                    placeholder="Password"
                                    required
                                    value={loginPassword}
                                    onChange={(e) => setLoginPassword(e.target.value)}
                                />
                            </div>
                            <input type="submit" value="Login" className="loginBtn" />
                        </form>
                        <form
                            className="signUpForm"
                            ref={registerTab}
                            encType="multipart/form-data"
                            onSubmit={registerSubmit}
                        >
                            <div className="signUpName">
                                <input
                                    type="text"
                                    placeholder="Name"
                                    required
                                    name="name"
                                    ref={nameRef}
                                />
                            </div>
                            <div className="signUpEmail">
                                <input
                                    type="email"
                                    placeholder="Email"
                                    required
                                    name="email"
                                    ref={emailRef}
                                />
                            </div>
                            <div className="signUpPassword">
                                <input
                                    type="password"
                                    placeholder="Password"
                                    required
                                    name="password"
                                    ref={passwordRef}
                                />
                                <div className="errorMsg">{errors.password}</div>
                            </div>
                            <input type="submit" value="Register" className="signUpBtn" />
                        </form>
                    </div >
                )}
                <ConfirmModal show={show} handleClose={handleClose} handleConfirm={handleConfirm} message={confirmMsg.current} />
            </div >
        </Fragment >
    )
}

export default Home;