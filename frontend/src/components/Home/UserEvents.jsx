import React from "react";
import moment from 'moment';
import { Table, Button } from 'react-bootstrap';

export default function UserEvents({ events, editHandler, deleteHandler }) {
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {events && events.length && events.map((event) => (
                    <tr key={event.id}>
                        <td>{event.name}</td>
                        <td>{moment(new Date(event.date)).utc().format('MMM Do YY')}</td>
                        <td>{event.description}</td>
                        <td>{event.price}</td>
                        <td><Button variant="outline-primary" onClick={() => editHandler(event)} size="sm">Edit</Button></td>
                        <td><Button variant="outline-danger" onClick={() => deleteHandler(event.id)} size="sm">Delete</Button></td>
                    </tr>
                ))}
            </tbody>
        </Table>
    )
}