import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    EVENT_REQUEST,
    EVENT_SUCCESS,
    CREATE_EVENT_REQUEST,
    CREATE_EVENT_SUCCESS,
    CREATE_EVENT_FAIL,
    UPDATE_EVENT_REQUEST,
    UPDATE_EVENT_SUCCESS,
    UPDATE_EVENT_FAIL,
    DELETE_EVENT_REQUEST,
    DELETE_EVENT_SUCCESS,
    DELETE_EVENT_FAIL,
} from "../constants/userConstants";
import axios from 'axios';

// Login user
export const login = (email, password) => async (dispatch) => {
    try {
        dispatch({ type: LOGIN_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.post(`/api/users/signin`, { email, password }, config);
        if (data.success) {
            dispatch({ type: LOGIN_SUCCESS, payload: data });
        } else {
            // dispatch({ type: LOGIN_FAIL, payload: data.message });
        }

    } catch (error) {
        // dispatch({ type: LOGIN_FAIL, payload: error.response.data.message });
    }
}

// Register user
export const register = (userData) => async (dispatch) => {
    try {
        dispatch({ type: REGISTER_USER_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.post(`/api/users/register`, userData, config);
        if (data.success) {
            dispatch({ type: REGISTER_USER_SUCCESS, payload: data });
        } else {
            dispatch({ type: REGISTER_USER_FAIL, payload: data.message });
        }
    } catch (error) {
        dispatch({ type: REGISTER_USER_FAIL, payload: error.response.data.message });
    }
}

// Register user
export const createEvent = (eventData) => async (dispatch) => {
    try {
        dispatch({ type: CREATE_EVENT_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.post(`/api/users/event/create`, eventData, config);
        if (data.success) {
            dispatch({ type: CREATE_EVENT_SUCCESS, payload: data });
        } else {
            dispatch({ type: CREATE_EVENT_FAIL, payload: data.message });
        }
    } catch (error) {
        dispatch({ type: CREATE_EVENT_FAIL, payload: error.response.data.message });
    }
}

// Edit user
export const editEvent = (eventData) => async (dispatch) => {
    try {
        dispatch({ type: UPDATE_EVENT_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.put(`/api/users/event/edit`, eventData, config);
        if (data.success) {
            dispatch({ type: UPDATE_EVENT_SUCCESS, payload: data });
        } else {
            dispatch({ type: UPDATE_EVENT_FAIL, payload: data.message });
        }
    } catch (error) {

    }
}

// Get user events
export const getEvent = (id) => async (dispatch) => {
    try {
        dispatch({ type: EVENT_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.post(`/api/users/event/get`, id, config);
        if (data.success) {
            dispatch({ type: EVENT_SUCCESS, payload: data });
        } else {
            dispatch({ type: REGISTER_USER_FAIL, payload: data.message });
        }
    } catch (error) {
        dispatch({ type: REGISTER_USER_FAIL, payload: error.response.data.message });
    }
}

// Delete user event
export const deleteEvent = (id) => async (dispatch) => {
    try {
        dispatch({ type: DELETE_EVENT_REQUEST });
        const config = { headers: { "Content-Type": "application/json" } };
        const { data } = await axios.put(`/api/users/event/delete`, id, config);
        if (data.success) {
            dispatch({ type: DELETE_EVENT_SUCCESS, payload: data });
        } else {
            dispatch({ type: DELETE_EVENT_FAIL, payload: data.message });
        }
    } catch (error) {

    }
}
