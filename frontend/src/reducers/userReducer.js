import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    // LOGIN_FAIL,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    EVENT_REQUEST,
    EVENT_SUCCESS,
    CREATE_EVENT_SUCCESS,
    CREATE_EVENT_REQUEST,
    UPDATE_EVENT_REQUEST,
    UPDATE_EVENT_SUCCESS,
    DELETE_EVENT_REQUEST,
    DELETE_EVENT_SUCCESS,
} from "../constants/userConstants";

export const userReducer = (state = { userData: {} }, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
        case REGISTER_USER_REQUEST:
            return {
                loading: true,
                isAuthenticated: false,
            };

        case LOGIN_SUCCESS:
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                isAuthenticated: true,
                userData: action.payload
            };
        default: return state;
    }
}

export const eventReducer = (state = { eventData: {} }, action) => {
    switch (action.type) {
        case EVENT_REQUEST:
        case CREATE_EVENT_REQUEST:
        case DELETE_EVENT_REQUEST:
            return {
                loadingEvent: true,
                success: false,
            };
        case EVENT_SUCCESS:
            return {
                loadingEvent: false,
                eventData: action.payload
            };
        case CREATE_EVENT_SUCCESS: {
            return {
                loadingEvent: false,
                createdData: action.payload
            }
        }
        case DELETE_EVENT_SUCCESS: {
            return {
                loadingEvent: false,
                deletedData: action.payload
            }
        }
        case UPDATE_EVENT_SUCCESS: {
            return {
                loadingEvent: false,
                message: action.payload.message,
                success: action.payload.success
            }
        }
        default: return state;
    }
}