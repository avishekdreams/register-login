import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './components/Home/Home';
import CreateEvents from './components/Home/CreateEvents';
import EditEvents from './components/Home/EditEvents';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/events/create" element={<CreateEvents />} exact />
          <Route path="/events/edit" element={<EditEvents />} exact />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
